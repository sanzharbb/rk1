<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;

class QuestionController extends Controller
{
    public function index() {
        $questions = Question::get();
        return view('index', ['questions' => $questions]);
    }

    public function newQuestion() {
        return view('newQuestion');
    }

    public function create(Request $request) {
        $question = new Question();
        $question->question = $request->get('question');
        $question->save();
        return redirect('/questions');
    }
}
